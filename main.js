const { app, BrowserWindow } = require('electron');

const createWindow = ()=>{
  let window = new BrowserWindow({
    darkTheme: true,
    webPreferences: {      
      offscreen: true,     
      plugins: true
    }
  })  
  window.loadURL('https://www.netflix.com/');
}
app.commandLine.appendSwitch("disable-gpu")
app.commandLine.appendArgument("disable-gpu")
app.whenReady().then(createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // No macOS é comum para aplicativos e sua barra de menu 
  // permaneçam ativo até que o usuário explicitamente encerre com Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (process.platform === 'linux') {
    app.disableHardwareAcceleration();
  }
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
})